package com.designpattern.Mediator;

import com.desginpattern.Colleagues.Colleague;

//interface to define the contract for concrete mediators
public interface Mediator {
	public void SendMessage(String message,Colleague colleague);
}
