package com.designpattern.Mediator;

import com.desginpattern.Colleagues.Colleague;
import com.desginpattern.Colleagues.ConcreteColleague1;
import com.desginpattern.Colleagues.ConcreteColleague2;

//concrete mediator provides logic for communication
public class ConcreteMediator implements Mediator{
	
	private ConcreteColleague1 colleague1;
	private ConcreteColleague2 colleague2;

	//setter for colleagu1
	public void setColleague1(ConcreteColleague1 colleague1) {
		this.colleague1 = colleague1;
	}

	//setter for colleague2
	public void setColleague2(ConcreteColleague2 colleague2) {
		this.colleague2 = colleague2;
	}

	@Override
	public void SendMessage(String message, Colleague colleague) {
		// TODO Auto-generated method stub
		
		if(colleague.equals(colleague1)) {
			colleague2.Receive(message);
		}
		else {
			colleague1.Receive(message);
		}
	}

}
