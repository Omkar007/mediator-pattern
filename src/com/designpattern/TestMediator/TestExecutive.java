package com.designpattern.TestMediator;

import com.desginpattern.Colleagues.ConcreteColleague1;
import com.desginpattern.Colleagues.ConcreteColleague2;
import com.designpattern.Mediator.ConcreteMediator;

public class TestExecutive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ConcreteMediator mediator=new ConcreteMediator();
		
		ConcreteColleague1 colleague1=new ConcreteColleague1(mediator);
		ConcreteColleague2 colleague2=new ConcreteColleague2(mediator);
		
		//registering colleagues objects with mediator
		mediator.setColleague1(colleague1);
		mediator.setColleague2(colleague2);
		
		colleague1.Send("Can you send some Beer Barells");
		colleague2.Send("Sure thing, John is on his way.");
		
		colleague2.Send("Do you have extra stock of chardonay? We've had a rush on them over here.");
		colleague1.Send("Just couple. Will send them back with John");
	}

}
