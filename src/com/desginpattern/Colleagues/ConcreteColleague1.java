package com.desginpattern.Colleagues;

import com.designpattern.Mediator.Mediator;

public class ConcreteColleague1 extends Colleague{
	public ConcreteColleague1(Mediator mediator) {
		super(mediator);
		// TODO Auto-generated constructor stub
	}

	public void Send(String message) {
		
		System.out.println("Concrete Colleague 1 sends message "+message);
		mediator.SendMessage(message, this);
	}
	
	public void Receive(String message) {
		System.out.println("Concrete Colleague 1 receives message "+message);
	}
	
}
