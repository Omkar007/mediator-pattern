package com.desginpattern.Colleagues;

import com.designpattern.Mediator.Mediator;

public class ConcreteColleague2 extends Colleague{

	public ConcreteColleague2(Mediator mediator) {
		super(mediator);
		// TODO Auto-generated constructor stub
	}
	
	public void Send(String message) {
		System.out.println("Concrete Colleague 2 sends message "+message);
		mediator.SendMessage(message, this);
	}
	
	public void Receive(String message) {
		System.out.println("Concrete Colleague 2 receives message "+message);
	}
	
}
