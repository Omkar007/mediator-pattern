package com.desginpattern.Colleagues;

import com.designpattern.Mediator.Mediator;

public abstract class Colleague {
	
	protected Mediator mediator;

	public Colleague(Mediator mediator) {
		super();
		this.mediator = mediator;
	}

}
